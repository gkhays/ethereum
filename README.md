## Ethereum Quick Start

Quick bash script to start go-ethereum. The default ports are fine.

RPC Port: default: 8545
Port default: 30303

```bash
# geth.sh
# Fetch local IP address into a variable.
MY_IP=$(ip addr list enp0s25 | grep "inet " | cut -d' ' -f6 | cut -d/ -f1)

# --testnet and --dev are mutually exclusive
~/src/ethereum/go-ethereum/build/bin/geth \
  --testnet --identity "IGBlock" \
  --rpc --rpcaddr "$MY_IP" --rpccorsdomain "*" \
  --nodiscover --rpcapi "db,eth,net,web3" console
```